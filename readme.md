## SiaClassic Explorer

## Related links
- [SiaClassic Indexer](https://gitlab.com/jinxianjo/siaclassic-indexer)
- [SiaClassic Hub](https://gitlab.com/jinxianjo/siaclassic-hub)
- [Laravel 5.4 docs](https://laravel.com/docs/5.4)


## Installation

### Web
- ``git clone https://gitlab.com/jinxianjo/siaclassic-explorer.git``
- ``npm i``
- ``npm run production``
- ``composer update``
- ``php artisan vendor:publish``
- ``php artisan key:generate``
- Make and edit .env config (check .env.example)
- ``php artisan migrate``
- Done!


## License

Available under [the MIT license](http://mths.be/mit).
